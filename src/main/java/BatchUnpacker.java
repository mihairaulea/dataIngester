import java.io.File;
import java.io.IOException;

public class BatchUnpacker {

    public void unpack(int index) throws IOException, InterruptedException {
        String tarGzPathLocation = PathUtils.getFile(index)+".gz";
        ProcessBuilder builder = new ProcessBuilder();
        builder.command("sh", "-c", String.format("gunzip -k %s", tarGzPathLocation));
        builder.directory(new File("/tmp"));
        Process process = builder.start();
        int exitCode = process.waitFor();
        assert exitCode == 0;
    }

    public void deleteUnpacked(int index) {
        String unpackedLocation = PathUtils.getFile(index);
        File myObj = new File(unpackedLocation);
        if (myObj.delete()) {
            System.out.println("Deleted the file: " + myObj.getName());
        } else {
            System.out.println("Failed to delete the file.");
        }
    }

}
