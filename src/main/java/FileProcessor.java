

import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.response.UpdateResponse;
import org.apache.solr.common.SolrInputDocument;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class FileProcessor {

    List<JSONObject> buffer = new LinkedList<>();

    // encrypt file for security access w admin rights
    // make custom docker image w another name for solr
    public void processFile(HttpSolrClient solr, int currentIndex) throws IOException {
        Files.lines(Paths.get(PathUtils.getFile(currentIndex)))
                .forEach(a -> {
                    //System.out.println(a);
                    JSONObject jo = new JSONObject(a);

                    ArrayList<String> interestingValues = Util.extractInterestingArrayValuesFromLine(jo);
                    interestingValues.addAll(Util.extractInterestingValuesFromLine(jo));

                    flushToSolr(interestingValues, solr, jo);
                });
    }

    private void flushToSolr(ArrayList<String> interestingValues, HttpSolrClient solr, JSONObject jo) {
        if (interestingValues.size() > 1) {
            //System.out.println(interestingValues);
            JSONObject newJson = new JSONObject();
            interestingValues.forEach(intVal -> newJson.put(intVal, jo.get(intVal)));
            // i should use the same field names everywhere
            if(newJson.has("job_company_name")) {
                newJson.put("company_name", newJson.get("job_company_name"));
                newJson.remove("job_company_name");
            }
            if(newJson.has("job_company_size")) {
                newJson.put("company_size", newJson.get("job_company_size"));
                newJson.remove("job_company_size");
            }
            if(newJson.has("job_company_industry")) {
                newJson.put("company_industry", newJson.get("job_company_industry"));
                newJson.remove("job_company_industry");
            }
            if(newJson.has("job_company_id")) {
                //newJson.put("company_id", newJson.get("job_company_id"));
                newJson.remove("job_company_id");
            }
            // add to schema
            // company_id,
            // twitter_username, github_username, linkedin_username (?),
            //

            buffer.add(newJson);
        }

        if (buffer.size() > 50_000) {
            System.out.println("flushing to solr");
            try {
                buffer.stream().forEach(bufferedDoc -> {
                    SolrInputDocument document = new SolrInputDocument();
                    bufferedDoc.keySet().stream().forEach(key -> {
                        document.addField(key, bufferedDoc.get(key));
                    });
                    try {
                        UpdateResponse response = solr.add(document);
                    }
                    catch (Exception e)
                    {
                        System.out.println(e.getMessage());
                    }
                });

                solr.commit();
                buffer = new LinkedList<>();
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
    }

}
