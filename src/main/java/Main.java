import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.HttpSolrClient;
import org.apache.solr.client.solrj.impl.XMLResponseParser;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;

public class Main {

    // delete all unpacked stuff
    // insert 300 into solr on main machine
    // measure how long it takes
    // simulate 200 clients asking for data

    /*
    https://billionleads.jekyo.app/solr/#/login
- name: SOLR_ADMIN_PASSWORD
              value: "6909286269"
            - name: SOLR_ENABLE_AUTHENTICATION
              value: "yes"
            - name: SOLR_ADMIN_USERNAME
              value: "billionleads"
     */

    public static void main(String[] args) throws Exception {
        try {
            // billionleads:6909286269@
            String urlString = "http://localhost:8983/solr/core/";//;
            HttpSolrClient solr = new HttpSolrClient.Builder(urlString).build();
            solr.setParser(new XMLResponseParser());

            System.out.println("CONNECTED");
            System.out.println(solr.ping());
            insertData(solr);
            //queryData(solr);
            //deleteAllUnpacked();
        }
        catch(Exception e) {
            System.out.println("_-----s___S__S");
            System.out.println(e.getMessage()+" ---");
        }
    }

    public static void queryData(HttpSolrClient solr) throws SolrServerException, IOException {
        SolrQuery query = new SolrQuery();
        query.set("q", "industry:internet");
        query.addFilterQuery("location_country:india");
        QueryResponse response = solr.query(query);

        SolrDocumentList docList = response.getResults();
        System.out.println(docList);
    }

    public static void deleteAllSolr() {
        SolrQuery query = new SolrQuery();
        query.set("q","*:*");
    }

    public static void unpackAll() throws Exception {
        for(int i=0;i<1243;i++)
            new BatchUnpacker().unpack(i);
    }

    public static void deleteAllUnpacked() throws Exception {
        for(int i=0;i<1243;i++)
            new BatchUnpacker().deleteUnpacked(i);
    }

    public static void insertData(HttpSolrClient solr) throws IOException {
        FileProcessor fileProcessor = new FileProcessor();
        for(int i=0;i<1243;i++) {
            fileProcessor.processFile(solr, i);
            System.out.println("------ have processed file " + String.valueOf(i));
        }
    }

}
