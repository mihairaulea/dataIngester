public class PathUtils {

    // server path /root/mega/data
    public static String getPathToData() {
        return "/media/mihai/Seagate/0-1243";//"/root/mega/data";
    }

    public static String getFile(int index) {
        return getPathToData()+"/part-"+padNumber(String.valueOf(index));
    }

    public static String padNumber(String indexString) {
        while(indexString.length()<5)
            indexString = '0'+ indexString;
        return indexString;
    }

}
