import java.util.ArrayList;
import java.util.Arrays;

public class Settings {

    // industry
    // job_title ---- this is wacky af
    // job_title_role
    // job_title_sub_role
    // job_title_levels -- array

    // interests
    // skills
    // experience
    // certifications
    // languages
    // education

    // summary
    // inferred_salary
    // inferred_years_experience
    // location_geo

    // linkedin_url
    // facebook_url
    // twitter_url
    // github_url

    // COMPANY
    // job_company_id
    // job_company_name
    // job_company_website
    // job_company_size
    // job_company_founded
    // job_company_industry

    // job_company_linkedin_url
    // job_company_twitter_url
    // job_company_facebook_url

    // job_company_location_geo

    // job_summary

    // 2GB just the index for:
    // industry, job_company_industry, location_country

    // READ THIS FROM CONFIG FILE
    public static ArrayList<String> getImportantValues() {
        return new ArrayList<>(Arrays.asList(
                "id",
                "full_name",
                "linkedin_username",
                "twitter_username",
                "github_username",
                "gender",
                "location_country",
                "location_locality",
                "job_title",
                "industry",
                "job_company_id",
                "job_company_name",
                "job_company_size",
                "job_company_industry",

                "job_company_linkedin_url",
                "job_company_twitter_url",
                "job_company_facebook_url",
                // TODO: add this to the solr index and add this to the svelte web app
                "job_company_website"
                /*
                "industry",
                // this should go in a separate place anyway
                "job_company_industry",
                "job_title_role",
                "job_title_sub_role",
                "location_country",
                "location_city",
                "inferred_years_experience",
                "inferred_salary",
                "job_summary", //array
                "job_title"

                 */
        ));
    }

    public static ArrayList<String> getImportantArrayValues() {
        return new ArrayList<>(Arrays.asList(
                //"skills",
                // some of these give up errors
                //"interests",
                //"experience",
                //"certifications",
                //"languages",
                // EMAILS AND PHONE NUMBERS SHOULD BE PROCESSED SEPARATELY
                "emails",
                "phone_numbers",
                // need the job title levels but i need to add it to index
                "job_title_levels"
                //"education",
                //
                //"job_title_levels"
        ));
    }

}
