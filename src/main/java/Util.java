import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static java.util.stream.Collectors.toCollection;

public class Util {

    // this only extracts strings -- arrays will be processed at a later date
    public static ArrayList<String> extractInterestingValuesFromLine(JSONObject jo) {
        return Settings.getImportantValues()
                .stream()
                .filter(value -> {
                    try
                    {
                        jo.get(value);
                        return true;
                    }
                    catch (Exception e) {
                        return false;
                    }
                })
                .filter(value -> jo.get(value) instanceof String)
                //.map(impValue -> jo.getString(impValue))
                // this just includes the type
                .collect(toCollection(ArrayList::new));
    }

    public static ArrayList<String> extractInterestingArrayValuesFromLine(JSONObject jo) {
        return Settings.getImportantArrayValues()
                .stream()
                .filter(value -> {
                    try
                    {
                        jo.get(value);
                        return true;
                    }
                    catch (Exception e) {
                        return false;
                    }
                })
                .filter(value -> jo.get(value) instanceof JSONArray)
                .filter(value -> ((JSONArray)jo.get(value)).length()>0)
                // this also includes the type
                .collect(toCollection(ArrayList::new));
    }



}
